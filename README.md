To convert index.md to index.html
```bash
pandoc index.md --template=index.html -s -V css:'css/tufte.css' --metadata title="Zak's Site" -f markdown -t html -o zwit.gitlab.io/index.html
```

To convert links.md to links.html
```bash
cat ~/files/research/links.md | \
	sed -e 's/.*\[TOC\]//p' | \
	pandoc -f markdown -t html \
	--template=links.html -s \
	-V css:'css/tufte.css' \
	-V toctitle:'Table of Contents' \
	--toc --toc-depth=1 \
	--metadata title="Links" \
	-o zwit.gitlab.io/links.html
```
