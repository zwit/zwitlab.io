## 2020-05-23

* Abbie planted a bunch more green onions, filling a long flower container and some on top of the garden tower too. I had her use the container that was supposed to be lettuce since it wasn't really growing. The garden took a beating last week with all the rain we got so I think they struggled with that. Cilantro also isn't doing well but some of the seeds have germinated so i'll probably just sew some more among the current seedlings.

* Max gave me some extra jalapeno and fox glove seedlings he had. Need to divide up the jalapeno seedlings and plant them in small pots. Will likely keep them inside under grow light until they are bigger.

![garden](.assets/2020-05-23_garden.jpg)
![seedlings](.assets/2020-05-23_seedlings.jpg)


## 2020-05-22

* Transplanted the 4 best tomato seedlings to 6" pots (2x sweeties, 2x sun golds)
* Thinned carrots which are doing well, probably about 2" in heigh at best
* A lot of the broccoli seedlings died as a result of me being distracted by studying and losing track and not watering them enough. Some of the broccoli 2nd round seedlings are still going well


## Tuesday, April 14, 2020

GQ did [an interview](https://www.gq.com/story/cal-newport-screen-time-coronavirus) with Cal Newport on "Surviving Screens and Social Media in Isolation." Some good reminders on being mindful of how we use our technology. Reminds me a lot of a book I read last year, Nicholas Carr's ["The Shallows: What the Internet Is Doing to Our Brains"](https://www.goodreads.com/book/show/9778945-the-shallows).

## Monday, April 13, 2020

I decided a good way to start writing on this blog is to use it to track my garden. Something to get the content flowing.

The seedlings are coming along nicely indoors. I thinned the broccoli. The tomatoes are almost to the point of needing to be thinned as well. And although you can't see them in this picture, the oregano and thyme have germinated as well. No signs of the carrots that I did outside in the garden tower.

![seedlings](.assets/2020-04-12_seedlings.jpg)
